<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Support\Str;

//$router->get('/key', function () {
//    return Str:: random('32');
//});
$router->get('/', function () use ($router) {
    return $router->app->version();
});
$router->post('api/v1/login', 'Auth\LoginController@verify');

$router->group(['prefix' => 'api/v1', 'middleware' => 'pbe.auth'], function ($router) {
    #to get all data
    $router->get('/songs', 'SongController@getAll');
    #to get Data by Id
    $router->get('/songs/{id}', 'SongController@getById');

    #to get all data
    $router->get('/playlists', 'PlaylistController@getAllPlaylist');
    #to get Data by Id
    $router->get('/playlists/{id}', 'PlaylistController@getPlaylistById');
    #to update data
    $router->post('/playlists', 'PlaylistController@createPlaylist');

    #to get Data by Id
    $router->get('/playlists/{id}/songs', 'PlaylistController@getAllSongById');
    #to create data
    $router->post('/playlists/{id}/songs', 'PlaylistController@createSongById');

    $router->group(['middleware' => 'pbe.superuser'], function ($router) {
        #to get all data
        $router->get('/users', 'UserController@getAll');
        #to get Data by Id
        $router->get('/users/{id}', 'UserController@getById');
        #to Insert data
        $router->post('/users', 'UserController@create');

        #hapus data lagu
        $router->delete('/songs/{id}', 'SongController@delete');
        #to create data
        $router->post('/songs', 'SongController@create');
        #to update data
        $router->put('/songs/{id}', 'SongController@update');

        #to get Playlist
        $router->get('/users/{id}/playlists', 'UserController@getPlaylistbyIdUser');
        #to create data
        $router->get('/users/{id}/playlists/{playlistId}/songs', 'UserController@getSongFromPlaylistByIdUser');

    });
});
