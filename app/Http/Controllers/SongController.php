<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\PbeBaseController;
use App\Models\Song;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class SongController extends PbeBaseController
{
    /**
     * function untuk mendapatkan semua data user
     * @return JsonResponse
     */
    public function getAll()
    {
        $songs = Song::all();
        return $this->succesResponse(['song' => $songs]);
    }

    /**
     * function mengambil satu data dari user berdasarkan primary key
     * @param $id
     * @return JsonResponse
     */
    public function getById($id)
    {
        $song = Song::find($id);
        if ($song === null) {
            throw new NotFoundHttpException();
        }
        return $this->succesResponse(['song' => $song]);
    }

    /**
     * function untuk menambah data di user
     * @param $id
     * @return JsonResponse
     */
    public function create()
    {
        /*validasi*/
        $validate = Validator::make(request()->all(), [
            'title' => 'required',
            'year' => 'required|numeric',
            'artist' => 'required',
            'gendre' => 'required',
            'duration' => 'required'
        ]);
        if ($validate->fails()) {
            return $this->failResponse((array)$validate->errors()->getMessages(), 400);
        }
        /*Jika tidak ada error yang terjadi*/
        $song = new Song();
        $song->title = request('title');
        $song->year = request('year');
        $song->artist = request('artist');
        $song->gendre = request('gendre');
        $song->duration = request('duration');
        $song->save();
        return $this->succesResponse(['song' => $song]);
    }

    /**
     * function untuk mengupdate data dari database
     * @param $id
     * @return JsonResponse
     */
    public function update($id)
    {
        $song = Song::find($id);
        if ($song === null) {
            throw new NotFoundHttpException();
        }
        /*validasi*/
        $validate = Validator::make(request()->all(), [
            'title' => 'required',
            'year' => 'required|numeric',
            'artist' => 'required',
            'gendre' => 'required',
            'duration' => 'required'
        ]);
        if ($validate->fails()) {
            return $this->failResponse((array)$validate->errors()->getMessages(), 400);
        }
        /*Jika tidak ada error yang terjadi*/
        $song->title = request('title');
        $song->year = request('year');
        $song->artist = request('artist');
        $song->gendre = request('gendre');
        $song->duration = request('duration');
        $song->save();
        return $this->succesResponse(['song' => $song]);
    }

    /**
     * Function delete songs by Id
     * @param $id
     * @return JsonResponse
     */
    public function delete($id)
    {
        $song = Song::find($id);
        if ($song === null) {
            throw new NotFoundHttpException();
        }
        /*
         *sebelum di delete maka di validasi dulu apakah id pada song terdapat
         * pada song_id di Playlist song
         */
        $playlistsong = Song::getIdSongfromPlaylistsong($id);
        if ($playlistsong !== null) {
            return $this->failResponse(['song' => 'Song tidak berhasil dihapus'], 400);
        }
        $song->delete();
        return $this->succesResponse(['song' => 'Song berhasil dihapus']);
    }

}

