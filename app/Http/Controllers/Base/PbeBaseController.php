<?php

namespace App\Http\Controllers\Base;

use Laravel\Lumen\Routing\Controller as BaseController;

abstract class PbeBaseController extends BaseController
{
    protected function succesResponse(array $data, int $httpCode = 200)
    {
        $response = [
            'status' => 'succes',
            'message' => 'Permintaan berhasil diproses',
            'data' => $data
        ];
        return response()->json($response, $httpCode);
    }

    protected function failResponse(array $data, int $httpCode)
    {
        $response = [
            'status' => 'failed',
            'message' => 'Permintaan gagal diproses',
            'data' => $data
        ];
        return response()->json($response, $httpCode);
    }

}
