<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\PbeBaseController;
use App\Models\Playlist;
use App\Models\Playlistsong;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PlaylistController extends PbeBaseController
{
    /**
     * function untuk mendapatkan semua data playlist
     * @return JsonResponse
     */
    public function getAllPlaylist()
    {
        $playlists = Playlist::getIdUser();
        return $this->succesResponse(['playlist' => $playlists]);
    }

    /**
     * function mengambil satu data dari playlist berdasarkan primary key
     * @param $id
     * @return JsonResponse
     */
    public function getPlaylistById($id)
    {

        $playlist = Playlist::getIdUser()->find($id);
        if ($playlist === null) {
            throw new NotFoundHttpException();
        }
        return $this->succesResponse(['playlist' => $playlist]);
    }

    /**
     * function untuk menambah data di playlist
     * @param $id
     * @return JsonResponse
     */
    public function createPlaylist()
    {
        /*validasi*/
        $validate = Validator::make(request()->all(), [
            'name' => 'required',
        ]);
        if ($validate->fails()) {
            return $this->failResponse((array)$validate->errors()->getMessages(), 400);
        }
        /*Jika tidak ada error yang terjadi*/
        $playlist = new Playlist();
        $playlist->name = request('name');
        $playlist->user_id = request()->user->id;
        $playlist->save();
        return $this->succesResponse(['playlist' => $playlist]);
    }

    /**
     * function untuk mendapatkan semua data playlist
     * @return JsonResponse
     */
    public function getAllSongById($id)
    {
        $playlist = Playlist::getIdUser()->find($id);
        if ($playlist === null) {
            throw new NotFoundHttpException();
        }
        $playlists = Playlistsong::getIdSong($id);
        return $this->succesResponse(['playlist' => $playlists]);
    }

    /**
     * function mengambil satu data dari playlist berdasarkan primary key
     * @param $id
     * @return JsonResponse
     */
    public function createSongById($id)
    {
        $playlist = Playlist::getIdUser()->find($id);
        if ($playlist === null) {
            throw new NotFoundHttpException();
        }
        /*validasi*/
        $validate = Validator::make(request()->all(), [
            'id' => 'required|Exists:songs'
        ]);
        if ($validate->fails()) {
            return $this->failResponse((array)$validate->errors()->getMessages(), 400);
        }
        /*Jika tidak ada error yang terjadi*/
        $playlistsong = new Playlistsong();
        $playlistsong->song_id = request('id');
        $playlistsong->playlists_id = $playlist->id;
        $playlistsong->save();
        return $this->succesResponse(['playlist' => $playlistsong]);
    }

}

