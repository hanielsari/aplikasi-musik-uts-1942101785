<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Base\PbeBaseController;
use App\Models\Playlist;
use App\Models\Playlistsong;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class UserController extends PbeBaseController
{
    /**
     * function untuk mendapatkan semua data user
     * @return JsonResponse
     */
    public function getAll()
    {
        $users = User::all();
        return $this->succesResponse(['user' => $users]);
    }

    /**
     * function mengambil satu data dari user berdasarkan primary key
     * @param $id
     * @return JsonResponse
     */
    public function getById($id)
    {
        $user = User::find($id);
        if ($user === null) {
            throw new NotFoundHttpException();
        }
        return $this->succesResponse(['user' => $user]);
    }

    /**
     * function untuk menambah data di user
     * @param $id
     * @return JsonResponse
     */
    public function create()
    {
        /*validasi*/
        $validate = Validator::make(request()->all(), [
            'email' => 'required',
            'password' => 'required',
            'role' => 'required',
            'fullname' => 'required'
        ]);
        if ($validate->fails()) {
            return $this->failResponse((array)$validate->errors()->getMessages(), 400);
        }
        /*Jika tidak ada error yang terjadi*/
        $user = new User();
        $user->email = request('email');
        $user->password = password_hash(request('password'), PASSWORD_DEFAULT);
        $user->role = request('role');
        $user->fullname = request('fullname');
        $user->save();
        return $this->succesResponse(['user' => $user]);
    }

    /**
     * Mengambil data playlists berdasarkan Id User
     * @param $id
     * @return JsonResponse
     */
    public function getPlaylistbyIdUser($id)
    {
        $user = User::find($id);
        //  $playlists = Playlist::find($id);
        if ($user === null) {
            throw new NotFoundHttpException();
        }
        $playlists = Playlist::getPlaylistbyIdUser($id);
        return $this->succesResponse(['playlist' => $playlists]);
    }

    /**
     * Mengambil semua lagu yang dimiliki oleh users berdasarkan playlistsId dan Id user
     * @param $id
     * @param $playlistId
     * @return JsonResponse
     */
    public function getSongFromPlaylistByIdUser($id, $playlistId)
    {
        $user = User::find($id);
        if ($user === null) {
            throw new NotFoundHttpException();
        }
        $playlist = Playlist::find($playlistId);
        if ($playlist === null) {
            throw new NotFoundHttpException();
        }
        if ($user->id !== $playlist->user_id) {
            throw new NotFoundHttpException();
        }
        $song = Playlistsong::getIdSong($playlistId);
        return $this->succesResponse(['song' => $song]);
    }

}

