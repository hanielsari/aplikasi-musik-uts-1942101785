<?php

namespace App\Http\Middleware;

use App\Exceptions\PbeNotAuthenticatedException;
use App\Models\User;
use Closure;

class PbeMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->header('api_token'))) {
            #kondisi ketika api_token tidak dikirim melalui header
            throw new PbeNotAuthenticatedException();
        }
        #kondisi token tidak kosong
        $token = request()->header('api_token');
        $user = User::where('api_token', '=', $token)->first();
        if ($user === null) {
            throw new PbeNotAuthenticatedException();
        }
        $this->User = $user;
        #kondisi ketika api_tokennya ada
        $request->user = $user;
        return $next($request);
    }
}
