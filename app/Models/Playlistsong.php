<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Playlistsong extends Model
{
    public static function getIdSong($id)
    {
        $song = DB::table('songs')
            ->join('playlistsongs', 'songs.id', '=', 'playlistsongs.song_id')
            ->join('playlists', 'playlists.id', '=', 'playlistsongs.playlists_id')
            ->where('playlistsongs.playlists_id', '=', $id)
            ->select('songs.id as Song_Id', 'title', 'year', 'artist', 'gendre', 'duration')
            ->get();
        return $song;
    }

}
