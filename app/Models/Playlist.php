<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Playlist extends Model
{
    public static function getIdUser()
    {
        // $request->user =$user;
        $id = request()->user->id;
        $take_id_user = self::where('user_id', $id)->get();
        return $take_id_user;
    }

    public static function getPlaylistbyIdUser($id)
    {
        $song = DB::table('playlists')
            ->join('users', 'playlists.user_id', '=', 'users.id')
            ->where('playlists.user_id', '=', $id)
            ->select('playlists.id', 'users.fullname', 'name as Playlist Name', 'user_id')
            ->get();
        return $song;
    }

}
