<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Song extends Model
{
    public static function getIdSongfromPlaylistsong($id)
    {
        $song = DB::table('songs')
            ->join('playlistsongs', 'songs.id', '=', 'playlistsongs.song_id')
            ->where('playlistsongs.song_id', '=', $id)
            ->first();
        return $song;
    }
}
